const path = require('path');
const webpack = require('webpack');

module.exports = {
	entry: [
		'webpack-dev-server/client?http://localhost:1337',
		'webpack/hot/dev-server',
		'./src/scripts/index'
	],
	output: {
		path: __dirname,
		filename: 'bundle.js',
		publicPath: '/static/'
	},
	cache: true,
	debug: true,
	devtool: '#source-map',
	module: {

		loaders: [
			// JSON Loader for PIXI
			{
				test: /\.json$/,
				include: path.join(__dirname, 'node_modules', 'pixi.js'),
				loader: 'json'
			},
			{
        test: /(node_modules|.js)/,
        loader: 'ify'
      },
			{
				test: /\.jsx?$/,
				loader: "babel-loader",
				include: [
					path.resolve(__dirname, "src"),
				],
				query: {
					presets: ['es2015', 'stage-0']
				}
			},
			{
				test: /\.scss$/,
				loader: 'style-loader!css-loader!sass-loader?sourceMap',
				include: path.join(__dirname, 'src/styles')
			},
			{
				test: /\.css$/,
				loader: 'style-loader!css-loader?sourceMap',
				include: path.join(__dirname, 'src/styles')
			}
		],
		postLoaders: [

			{
				test: /\.js$/,
				include: path.join(__dirname, 'node_modules', 'pixi.js'),
				loader: 'transform/cacheable?brfs'
			},


		]

	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin()
	]
};
