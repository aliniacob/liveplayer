import PIXI from 'pixi.js';
import helper from './helpers';

const imgProcess = {

	resizeImage: (texture, maxW, maxH, cover, ctxData = false) => {
		const tempCanvas = document.createElement('canvas');
		const ctx = tempCanvas.getContext('2d');

		let w = texture.width;
		let h = texture.height;

		let ratio = cover ?  Math.max(maxW / w, maxH / h) : Math.min(maxW / w, maxH / h);

		let newW = w * ratio;
		let newH = h * ratio;

		tempCanvas.width = newW;
		tempCanvas.height = newH;

		ctx.drawImage(texture.baseTexture.source, 0, 0, newW, newH);

		if (ctxData) {
			return ctx.getImageData((newW - maxW) / 2, (newH - maxH) / 2, maxW, maxH);
		}

		return new PIXI.Texture.fromCanvas(tempCanvas, 'LINEAR');
	},

	generateBackground: (textures, w, h, type, shuffle = true) => {
		let bg = {
			'blurro': () => {
				const cv = document.createElement('canvas');
				const ctx = cv.getContext('2d');

				let textureArray = [...textures];
				let len = textureArray.length;
				let idealPics = len + len - Math.ceil(Math.sqrt(len));

				if (shuffle) {
					textureArray = helper.shuffleArray(textureArray)

					if (len < idealPics) {
						for (let i = 0; i < idealPics - len; i += 1) {
							textureArray.push(textures[Math.floor(Math.random() * len)]);
						}
					}

				}

				let ratio = Math.sqrt(textureArray.length)

				cv.width = w * ratio;
				cv.height = h * ratio;

				for (let i = 0; i < textureArray.length; i += ratio) {
					for (let j = 0; j < ratio; j += 1) {
						let x = w * j;
						let y = h * i / ratio;
						let imgData = imgProcess.resizeImage(textureArray[i + j], w, h, true, true);

						ctx.putImageData(imgData, x, y);
					}
				}

				return {
					'texture': new PIXI.Texture.fromCanvas(cv, 'LINEAR'),
					'shuffledArr': textureArray
				};
			}
		}

		return bg[type]();
	}
}

export default imgProcess;
