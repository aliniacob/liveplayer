const helper = {
	'shuffleArray': (arr) => {
		let tempArr = arr
		let cInx = tempArr.length;
		let tempVal = null;
		let rInx = null;

		while (0 !== cInx) {

			rInx = Math.floor(Math.random() * cInx);
			cInx -= 1;

			tempVal = tempArr[cInx];
			tempArr[cInx] = tempArr[rInx];
			tempArr[rInx] = tempVal;
		}

		return tempArr;
	},

	'randomStep': (frac, total) => {
		let arr = Array(total / frac).fill(0).map((a, i) => {return i + 1});

		return arr[Math.round(Math.random() * (arr.length - 1))];
	}
}

export default helper;
