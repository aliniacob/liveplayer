export default class ProgressBar {
	constructor(width, height, type) {
		this.width = width;
		this.height = height;

		this.wrapper = document.createElement('div');
		this.progresser = document.createElement('div');

		this.wrapper.className = 'progress-bar';
		this.progresser.className = 'progresser';
		this.wrapper.appendChild(this.progresser);

		this[type]();
	}

	bar() {
		this.wrapper.classList.add('bar');
		return this.wrapper;
	}

	update(val) {
		let step = val / 100;
		this.progresser.style.transform = `scale(${step})`;
	}
}
