import LiveSlider from './lib/controller';
require('../styles/style.scss');

// import PIXI from '../../pixi.js-dev';

new LiveSlider({
	'wrapper': 'photoWrapper',													// The main wrapper that will hold the LiveHeader,
	'template': 'blurro',																// Chosen template
	'effects': {																				// Chosen effects
		'filters': {																			// Filters applied per photo (ex. sepia, blur, ...)
			'collection': ['RedFilter'],									// Enumerate your prefered filters (ex. ['sepia', 'blur', ...])
			'filterSequence': 'random'											// Choose how filters should be applied (random or following the upper selection order)
		},
	}
});
