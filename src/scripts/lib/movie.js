import LiveView from './liveView';
import PIXI from 'pixi.js/';
import helper from '../misc/helpers';
import TWEEN from '../thirdParty/tween.js';
import imgProcess from '../misc/imageProcesor';
import * as Filters from '../shaders/';

export default class Movie {
	constructor(config) {

		this.controller = config.controller;
		this.events = config.events;
		this.renderer = config.renderer;
		this.stage = config.stage;
		this.ticker = config.ticker;
		this.tw = TWEEN;

		this.events.on('enviroment-ready', () => this.renderEnviroment());
	}



	renderEnviroment() {

		let bg = this.controller.graphics.enviromentSprites[0];
		this.controller.sounds.loadedSounds[0].play();
		let filter = Filters[this.controller.effects.filters.collection[0]];
		this.ticker.add((time) => {
			    filter.uniforms.customUniform += 0.04;
		});
		bg.filters = [filter];

		this.stage.addChild(bg);

		const startTween = () => {

			let psz = {
				'w': this.controller.playerSize.width,
				'h': this.controller.playerSize.height
			};
			let ssz = {
				'w': bg.texture.width,
				'h': bg.texture.height
			};

			var tween = new TWEEN.Tween(bg)
			.to({
				px: -1 * (ssz.w / psz.w) + helper.randomStep(psz.w, ssz.w),
				py: -1 * (ssz.h / psz.h) + helper.randomStep(psz.h, ssz.h)
			}, 3000)
			.onComplete(() => {
				startTween();
			})
			.delay(1000)
			.easing(TWEEN.Easing.Quadratic.InOut)
			.start();
		};
		startTween();
		this.startRendering();
	}

	startRendering() {

		this.ticker.add(() => {
			this.renderer.render(this.stage);
			this.tw.update();
		});

		this.ticker.start();
	}
}
