import PIXI from 'pixi.js';
import imgProcess from '../misc/imageProcesor';
import Movie from './movie';

export default class LiveView {
	constructor(controller) {
		this.controller = controller;
		this.events = this.controller.events;
		this.ticker = new PIXI.ticker.Ticker();

		let rendererOptions = [
			this.controller.playerSize.width,
			this.controller.playerSize.height,
			{
				'autoResize': true,
				'resolution': 1
			},	false
		];

		this.renderer = new PIXI.autoDetectRenderer(...rendererOptions);
		this.controller.playerContainer.appendChild(this.renderer.view);
		this.stage = new PIXI.Container();

		this.initRenderer();
		this.bindListeners();
	}

	initRenderer() {
		this.events.on('assets-ready', () => {
			return new Movie(this);
		});
	}

	updateEnviroment(sz) {
		let template = this.controller.options.settings.template;
		let shuffledArray = this.controller.graphics.shuffledArray;

		let sprite = this.controller.graphics.enviromentSprites[0];
		let texture = imgProcess.generateBackground(shuffledArray, sz.width, sz.height, template, false);

		sprite.texture = texture.texture;
		sprite.cx = sz.width;
		sprite.cy = sz.height;
	}

	updateTextures(size, initialSize) {
		this.controller.graphics.sprites.forEach((sprite, i) => {
			let baseTexture = this.controller.graphics.rawTextures[i];

			sprite.cx = size.width;
			sprite.cy = size.height;

			sprite.texture = imgProcess.resizeImage(baseTexture, size.width, size.height, false);

			sprite.texture.requiresUpdate = true;
			sprite.texture.update();
		});

		this.controller.initialPlayerSize = size;
		this.events.emit('sprites-updated', size);
	}

	resizeView(size) {
		this.renderer.resize(size.width, size.height);
	}

	bindListeners() {
		this.events.on('window-resize', (size, initialSize) => {
			this.resizeView(size);
			this.updateTextures(size, initialSize);
		});

		this.events.on('sprites-updated', (sz) => {
			this.updateEnviroment(sz);
		});
	}

}
