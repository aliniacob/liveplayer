import PIXI from 'pixi.js';
import pixiAudio from 'pixi-audio';
import LiveView from './liveView';
import ProgressBar from '../misc/progresBar';
import ee from 'event-emitter';
import imgProcess from '../misc/imageProcesor';



PIXI.GC_MODES.DEFAULT = PIXI.GC_MODES.AUTO;
PIXI.utils._saidHello = true;



//PIXI DisplayObject wrapper to use relative values
Object.defineProperties(PIXI.DisplayObject.prototype, {
	px: {
			get: function ()
			{
					return 1 / (this.cx / this.position.x) || 0;
			},
			set: function (value)
			{
					this.position.x = value * this.cx;
			}
	},

	py: {
			get: function ()
			{
					return 1 / (this.cy / this.position.y) || 0;
			},
			set: function (value)
			{
					this.position.y = value * this.cy;
			}
	}
});

let defaultSettings = {
	'wrapper': '',																			// The main wrapper that will hold the LiveHeader,
	'template': '',																			// Chosen template
	'effects': {																				// Chosen effects
		'filters': {																			// Filters applied per photo (ex. sepia, blur, ...)
			'collection': ['a', 'b', 'c'],									// Enumerate your prefered filters (ex. ['sepia', 'blur', ...])
			'filterSequence': 'random'											// Choose how filters should be applied (random or following the upper selection order)
		},
	}
};

export default class LiveSlider{

	constructor(ops) {
		this.events = ee({});
		this.options = {'settings': {}};

		this.graphics = {
			'sprites': [],
			'shuffledArr': [],
			'rawTextures': [],
			'assetsStrings': [],
			'enviromentSprites': []
		};

		this.sounds = {
			'soundAssets': [],
			'loadedSounds': []
		};

		this.initPlayerSettings(Object.assign(defaultSettings, ops));
	}

	initPlayerSettings(settings) {

		// Set player wrapper
		this.options.settings.domWrapper = document.getElementById(settings.wrapper);

		// Set player width and height
		this.options.settings.size = {
			'width': this.options.settings.domWrapper.offsetWidth,
			'height': this.options.settings.domWrapper.offsetHeight
		};

		this.options.settings.initialSize = {
			'width': this.options.settings.domWrapper.offsetWidth,
			'height': this.options.settings.domWrapper.offsetHeight
		};

		// Set player effects
		this.options.settings.effects = settings.effects;
		this.options.settings.template = settings.template;

		this.graphics.assetsStrings = this.getGraphicAssets();
		this.sounds.soundAssets = this.getSoundAssets();
		this.preloadResources();
	}

	get playerContainer() {
		return this.options.settings.domWrapper;
	}

	set playerContainer(newContainer) {
		this.options.settings.domWrapper = document.getElementById(newContainer);
	}



	get graphicAssets() {
		return this.graphics.assetsStrings;
	}

	set graphicAssets(assetString) {
		this.graphics.assetsStrings = assetString;
	}

	get initialPlayerSize() {
		return this.options.settings.initialSize;
	}

	set initialPlayerSize(newSize) {
		this.options.settings.initialSize = {
			'width': newSize.width,
			'height': newSize.height
		};
	}


	get playerSize() {
		return this.options.settings.size;
	}


	set playerSize(size) {
		this.options.settings.size = {
			'width': size.width,
			'height': size.height
		};
	}

	get effects() {
		return this.options.settings.effects;
	}

	set effects(newEffects) {
		return this.options.settings.effects = newEffects;
	}

	getGraphicAssets() {
		const graphics = this.playerContainer.querySelectorAll('i[media-path]');

		return Array.from(graphics).map(item => {
			return [item.getAttribute('media-path'), item.innerText]
		});
	}

	getSoundAssets() {
		const sounds = this.playerContainer.querySelectorAll('s[media-path]');

		return Array.from(sounds).map(sound => {
			return [sound.getAttribute('media-path')];
		});
	}


	prepareTextures() {
		this.graphics.sprites = [];
		this.graphics.rawTextures.sort((a, b) => {
			return a.name.split('---')[1] - b.name.split('---')[1];
		}).forEach(texture => {
			let ss = this.playerSize;
			let tempTexture = imgProcess.resizeImage(texture, ss.width, ss.height, false);
			let sprite = new PIXI.Sprite(tempTexture);

			sprite.anchor.y = 0.5;
			sprite.anchor.x = 0.5;

			sprite.cx = ss.width;
			sprite.cy = ss.height;

			sprite.pivot.y = 0.5;
			sprite.pivot.x = 0.5;

			this.graphics.sprites.push(sprite);
		});

		this.initLiveViewer();

		this.events.emit('assets-ready');

		this.generateEnviroment();

	}

	generateEnviroment() {
		let template = this.options.settings.template;
		let rawTextures = this.graphics.rawTextures;

		let mW = this.playerSize.width;
		let mH = this.playerSize.height;

		let bg = imgProcess.generateBackground(rawTextures, mW, mH, template, true);
		let sprite = new PIXI.Sprite(bg.texture);

		sprite.anchor.y = 0;
		sprite.anchor.x = 0;

		sprite.cx = mW;
		sprite.cy = mH;

		sprite.pivot.y = 0.5;
		sprite.pivot.x = 0.5;

		this.graphics.enviromentSprites.push(sprite);
		this.graphics.shuffledArray = bg.shuffledArr;

		this.events.emit('enviroment-ready');
	}

	preloadResources() {
		let loader = new PIXI.loaders.Loader();

		this.progress = new ProgressBar(this.playerSize.width, 20, 'bar');
		this.playerContainer.appendChild(this.progress.wrapper);

		this.graphics.assetsStrings.forEach((asset, i) => {
			loader.add(`${asset[1]}//asset---${i}`, asset[0]);
		});

		this.sounds.soundAssets.forEach((sound, i) => {
			loader.add(`sound---${i}`, sound[0]);
		});

		loader
		.on('progress', (loader, res) => {
			this.progress.update(loader.progress);
		})
		.on('load', (loader, res) => {
			if (res.texture) {
				res.texture.name = res.name;
				this.graphics.rawTextures.push(res.texture);
			} else {
				this.sounds.loadedSounds.push(PIXI.audioManager.getAudio(res.name));
			}
		})
		.once('complete', (a, b) => {
			this.progress.wrapper.classList.add('hide');
			loader.reset();
			this.prepareTextures();
			this.addListeners();
		})
		.load();
	}

	initLiveViewer() {
		return new LiveView(this);
	}

	addListeners() {
		window.addEventListener('resize', () => {
			this.playerSize = {
				'width': this.options.settings.domWrapper.offsetWidth,
				'height': this.options.settings.domWrapper.offsetHeight
			};
			this.events.emit('window-resize', this.playerSize, this.initialPlayerSize);
		});
	}
}
