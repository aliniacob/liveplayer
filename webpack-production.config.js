var fs = require('fs');
var webpack = require('webpack');
var path = require('path');

// definePlugin takes raw strings and inserts them, so you can put strings of JS if you want.
var definePlugin = new webpack.DefinePlugin({
    "process.env": {
        "NODE_ENV": JSON.stringify('production'),
        "CLIENT": 'true',
        "SERVER": 'false'
    }
});

module.exports = {
	resolve: {
	    extensions: ['', '.js']
	},

	entry: [
	    './src/scripts/index'
	],
	output: {
	    path: path.join(__dirname, '/static/public/build'),
	    filename: 'bundle.js'
	},
	module: {
		loaders: [
			// JSON Loader for PIXI
			{
				test: /\.json$/,
				include: path.join(__dirname, 'node_modules', 'pixi.js'),
				loader: 'json',
			},
			{
				loader: "babel-loader",
				include: [
					path.resolve(__dirname, "src"),
				],
				test: /\.jsx?$/,
				query: {
					stage: 0,
					externalHelpers: true
				}
			}
		]
	},
	plugins: [
		definePlugin,
		new webpack.optimize.OccurenceOrderPlugin(true),
		new webpack.optimize.UglifyJsPlugin({
		    beautify: true,
		    verbose: false
		}),
		function () {
		    this.plugin("done", function (stats) {
		        var jj = stats.toJson({
		            errorDetails: false,
		            reasons: false,
		            source: false,
		            chunks: false,
		            modules: false,
		            children: false
		        });
		    });
		}
	]
};
